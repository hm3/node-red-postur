# node-RED-postur

This is a simple workflow which uses API of Postur.is (the API itself is not exposed officialy, so be careful to not flood it).

The workflow consists of 2 branches:

* http request based
* incoming param based

### 1. HTTP request based

The node accepts param `track`, which is a valid tracking number of the parcel. 

The call is accesible: 

`http://127.0.0.1:1880/postur?track=tracking_number`

### 2. incoming param based

Basically, everything that sends valid tracking number can be converted into a global or valid `msg.string` of this branch. In the wokflow it is ` package_code` in `query_param` and it should be changed to valid tracking number. 

The output is always json. 

##### TODO

* change input to something different like email based, tcp, udp based etc
* apply template in order to provision some nice looking output (via email, or sms, maybe whatsapp message - but that would required a bot)
